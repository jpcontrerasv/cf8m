<?php include 'header.php' ?>
    
<div class="w-100 position-fixed barra text-right d-flex justify-content-end align-items-center">
    
    <p>¡Ya quiero inscribirme! :)</p>
    
    <p>
        <a href="https://goo.gl/forms/6LcO6yXmbdlgbGlY2" target="_blank" class="btn btn-primary">¡Llévame al formulario!</a>
    </p>
</div>
<div class="container">
    
    <div class="row">
        
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 text-center img mb-5 d-block mx-auto">
            <img src="/img/lgoo.png" alt="" class="mb-5">
            
            <img src="/img/8m_morado.png" alt="" class="mt-5 logo-8m d-xl-block d-md-block d-sm-none d-none mx-auto " >
            
        </div>
        
        <div class="col-xl-8 offset-xl-1 col-lg-8 offset-lg-1 col-md-8 offset-md-1 col-sm-12 offset-sm-0 col-12 offset-0 ">
            <p>El <strong>Encuentro Nacional de Mujeres que Luchan</strong> se realizará en la Universidad de Santiago de Chile el 8 y 9 de Diciembre del 2018, para discutir cuáles deben ser las demandas centrales que llevaremos a la Huelga del 8 de Marzo del 2019 y cómo caminamos hacia ella feministas y movimientos sociales</p>
            
            <p>La inscripción al encuentro tiene un valor mínimo de $2000 pesos, pero si puedes aportar más que eso estaremos muy <u title="Muy" >agradecidas</u> pues necesitamos financiar alojamiento para asistentes, comida, papelería, entre muchas otras cosas.</p>
            
            <img src="img/8m_morado.png" alt="" class="d-xl-none d-lg-none d-md-none d-sm-block d-block logo-8m mx-auto mb-5" >
            
            <br><br><br><br><br><br>
            
            
        </div>

    </div>

</div>

<?php include 'footer.php' ?>

  